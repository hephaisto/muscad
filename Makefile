PYTHONPATH := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

.PHONY: clean examples

tests:
	pytest

format:
	black . --target-version py36 -l 79

clean:
	find ./examples -name "*.scad" -delete
	find ./examples -name "*.stl" -delete

mrproper: clean
	find . -name '.pytest_cache' -type d -exec rm -r {} +
	find . -name '__pycache__' -type d -exec rm -rf {} +

examples:
	${MAKE} -C $@ PYTHONPATH=${PYTHONPATH}

coverage-tests:
	coverage run -m pytest
	coverage html
	xdg-open htmlcov/index.html

pylint:
	pylint muscad/

mypy:
	mypy --show-error-codes muscad/

checklist: format mypy coverage-tests

release-patch: checklist
	bumpversion patch setup.py
	python setup.py sdist
	twine upload dist/muscad-*.tar.gz
	rm dist/muscad-*.tar.gz

release-minor: checklist
	bumpversion minor setup.py
	python setup.py sdist
	twine upload dist/muscad-*.tar.gz
	rm dist/muscad-*.tar.gz

release-major: checklist
	bumpversion major setup.py
	python setup.py sdist
	twine upload dist/muscad-*.tar.gz
	rm dist/muscad-*.tar.gz