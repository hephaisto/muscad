difference() {
  union() {
    // base
    translate(v=[0, 0, -6]) 
    linear_extrude(height=6, center=false, convexity=10, twist=0.0, scale=1.0) 
    hull() 
    {
      translate(v=[-14.1, -14.1, 0]) 
      circle(d=2, $fn=15);
      translate(v=[-10.1, 48.1, 0]) 
      circle(d=10, $fn=78);
      translate(v=[48.1, -10.1, 0]) 
      circle(d=10, $fn=78);
      translate(v=[43.1, 15.1, 0]) 
      circle(d=20, $fn=157);
      translate(v=[15.1, 43.1, 0]) 
      circle(d=20, $fn=157);
    }
    // ball_holder
    translate(v=[6.9, 6.9, -22.5]) 
    cylinder(h=33, d=44, $fn=345, center=true);
    // fillet
    intersection() {
      translate(v=[6.9, 6.9, -6.0]) 
      rotate_extrude(angle=360, $fn=2826) 
      translate(v=[22.0, 0, 0]) 
      mirror(v=[0, 1, 0]) 
      difference() {
        translate(v=[2.0, 2.0, 0]) 
        square(size=[4, 4], center=true);
        translate(v=[4.0, 4.0, 0]) 
        circle(d=8, $fn=62);
      }
      // volume
      translate(v=[19.0, 19.0, -8.0]) 
      cube(size=[68.2, 68.2, 4], center=true);
    }
  }
  // z_extrusion
  // profile
  %linear_extrude(height=60, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // y_extrusion
  %translate(v=[0.0, 75.1, 15.1]) 
  rotate(a=[270, 0, 180]) 
  // profile
  linear_extrude(height=60, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // x_extrusion
  %translate(v=[75.1, 0.0, 15.1]) 
  rotate(a=[270, 0, 90]) 
  // profile
  linear_extrude(height=60, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // cast_bracket
  %translate(v=[33.1, 33.1, 15.1]) 
  difference() {
    // body
    // volume
    cube(size=[36.0, 36.0, 28.0], center=true);
    // clearance
    translate(v=[18.0, 18.0, 0.0]) 
    rotate(a=[0, 0, 45]) 
    // volume
    cube(size=[45.0, 45.0, 28.04], center=true);
  }
  // right_bolt
  translate(v=[43.1, 0.0, 1.19]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // front_bolt
  translate(v=[0.0, 43.1, 1.19]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // center_bolt
  translate(v=[0.0, 0.0, 1.44]) 
  union() {
    // thread
    cylinder(h=20, d=8.372, $fn=65, center=true);
    // head
    translate(v=[0, 0, -13.43]) 
    cylinder(h=6.9, d=15.4, $fn=120, center=true);
    // head_clearance
    translate(v=[0, 0, -66.86]) 
    cylinder(h=100, d=15.4, $fn=120, center=true);
  }
  // squash_ball
  #translate(v=[6.9, 6.9, -38.0]) 
  sphere(d=40, $fn=314);
}