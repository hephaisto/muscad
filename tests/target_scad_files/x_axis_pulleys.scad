difference() {
  // body
  difference() {
    // volume
    translate(v=[0.0, 0.685, -5.64]) 
    cube(size=[61.56, 20.77, 20.78], center=true);
    // back_bottom_chamfer
    translate(v=[-30.8, -7.7, -14.03]) 
    rotate(a=[270, 0, 0]) 
    rotate(a=[0, 90, 0]) 
    linear_extrude(height=61.6, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
  }
  // center_pulleys_bolt
  translate(v=[0, 10.17, 0.0]) 
  rotate(a=[270, 0, 0]) 
  union() {
    // thread
    cylinder(h=16, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -9.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -60.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    translate(v=[0, 0, 6.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // inline_nut_clearance
    hull() 
    {
      translate(v=[0, 0, 6.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      translate(v=[0, 0, 16.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
  }
  // x_bearing_top_left
  mirror(v=[1, 0, 0]) 
  // x_bearing_top_right
  translate(v=[15.65, 0.0, 22.0]) 
  rotate(a=[0, 270, 90]) 
  union() {
    // base
    // volume
    cube(size=[34.3, 30.3, 22.3], center=true);
    // bolts
    union() {
      translate(v=[-12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[-12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
    }
    // rod_clearance
    rotate(a=[90, 0, 0]) 
    hull() 
    {
      cylinder(h=234.3, d=10, $fn=78, center=true);
      translate(v=[0, 10, 0]) 
      cylinder(h=234.3, d=10, $fn=78, center=true);
    }
  }
  // x_bearing_bottom
  translate(v=[0.0, 0.0, -22.0]) 
  rotate(a=[0, 270, 90]) 
  union() {
    // base
    // volume
    cube(size=[34.3, 30.3, 22.3], center=true);
    // bolts
    union() {
      translate(v=[-12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[-12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
    }
    // rod_clearance
    rotate(a=[90, 0, 0]) 
    hull() 
    {
      cylinder(h=234.3, d=10, $fn=78, center=true);
      cylinder(h=234.3, d=10, $fn=78, center=true);
    }
  }
  // left_pulley
  translate(v=[-23.35, 0.0, -6.35]) 
  union() {
    // body
    cylinder(h=10.7, d=15.4, $fn=120, center=true);
    // clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[0.0, 10.0, 0.0]) 
    cube(size=[15.4, 20, 10.7], center=true);
  }
  // right_pulley
  mirror(v=[1, 0, 0]) 
  // left_pulley
  translate(v=[-23.35, 0.0, -6.35]) 
  union() {
    // body
    cylinder(h=10.7, d=15.4, $fn=120, center=true);
    // clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[0.0, 10.0, 0.0]) 
    cube(size=[15.4, 20, 10.7], center=true);
  }
  // left_pulley_shaft
  translate(v=[-23.35, 0.0, -8.03]) 
  rotate(a=[180, 0, 0]) 
  union() {
    // thread
    cylinder(h=20, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -11.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -62.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 7.7]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
  }
  // right_pulley_shaft
  mirror(v=[1, 0, 0]) 
  // left_pulley_shaft
  translate(v=[-23.35, 0.0, -8.03]) 
  rotate(a=[180, 0, 0]) 
  union() {
    // thread
    cylinder(h=20, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -11.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -62.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 7.7]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
  }
}