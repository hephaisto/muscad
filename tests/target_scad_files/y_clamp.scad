difference() {
  // body
  difference() {
    // volume
    translate(v=[-6.3, 0.0, 0.0]) 
    cube(size=[12.6, 38.36, 34.8], center=true);
    // front_top_chamfer
    translate(v=[-12.62, 17.18, 15.4]) 
    rotate(a=[90, 0, 0]) 
    rotate(a=[0, 90, 0]) 
    linear_extrude(height=12.64, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
    // front_bottom_chamfer
    translate(v=[-12.62, 17.18, -15.4]) 
    rotate(a=[0, 90, 0]) 
    linear_extrude(height=12.64, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
    // back_bottom_chamfer
    translate(v=[-12.62, -17.18, -15.4]) 
    rotate(a=[270, 0, 0]) 
    rotate(a=[0, 90, 0]) 
    linear_extrude(height=12.64, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
    // back_top_chamfer
    translate(v=[-12.62, -17.18, 15.4]) 
    rotate(a=[180, 0, 0]) 
    rotate(a=[0, 90, 0]) 
    linear_extrude(height=12.64, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
  }
  // y_bearing
  #rotate(a=[270, 0, 180]) 
  union() {
    // outer
    cylinder(h=30.6, d=21.2, $fn=166, center=true);
    // rod_clearance
    cylinder(h=70.6, d=14.4, $fn=113, center=true);
  }
  // bolts
  union() {
    // y_clamp_bolt_bottom_front
    mirror(v=[0, 0, 1]) 
    // y_clamp_bolt_top_front
    translate(v=[2.0, 14.68, 13.0]) 
    rotate(a=[0, 90, 0]) 
    union() {
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 6.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[15.0, 0.0, 6.68]) 
      cube(size=[30, 6.089, 2.8], center=true);
    }
    // y_clamp_bolt_bottom_back
    mirror(v=[0, 0, 1]) 
    // y_clamp_bolt_top_back
    mirror(v=[0, 1, 0]) 
    // y_clamp_bolt_top_front
    translate(v=[2.0, 14.68, 13.0]) 
    rotate(a=[0, 90, 0]) 
    union() {
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 6.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[15.0, 0.0, 6.68]) 
      cube(size=[30, 6.089, 2.8], center=true);
    }
    // y_clamp_bolt_top_front
    translate(v=[2.0, 14.68, 13.0]) 
    rotate(a=[0, 90, 0]) 
    union() {
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 6.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[15.0, 0.0, 6.68]) 
      cube(size=[30, 6.089, 2.8], center=true);
    }
    // y_clamp_bolt_top_back
    mirror(v=[0, 1, 0]) 
    // y_clamp_bolt_top_front
    translate(v=[2.0, 14.68, 13.0]) 
    rotate(a=[0, 90, 0]) 
    union() {
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      rotate(a=[0, 0, 90]) 
      translate(v=[0, 0, 6.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      rotate(a=[0, 0, 90]) 
      translate(v=[15.0, 0.0, 6.68]) 
      cube(size=[30, 6.089, 2.8], center=true);
    }
  }
}